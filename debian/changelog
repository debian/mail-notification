mail-notification (5.4.dfsg.1-15) UNRELEASED; urgency=medium

  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:56:04 +0200

mail-notification (5.4.dfsg.1-14) unstable; urgency=medium

  * Switch to gstreamer 1.0 for audio playback (closes: #800023).
  * Standards-Version 3.9.6, no change required.
  * Add VCS information.
  * Drop obsolete Lintian override for the extra license file in the on-
    line help.

 -- Stephen Kitt <skitt@debian.org>  Sat, 26 Sep 2015 11:47:32 +0200

mail-notification (5.4.dfsg.1-13) unstable; urgency=medium

  * Build-depend on libebook1.2-dev (closes: #775620).

 -- Stephen Kitt <skitt@debian.org>  Sun, 18 Jan 2015 11:39:34 +0100

mail-notification (5.4.dfsg.1-12) unstable; urgency=medium

  * Merge patch from Ubuntu to stop linking with bsd-compat, which is no
    longer provided by glibc (closes: #753155); thanks to Iain Lane for
    the patch and to Michael Tautschnig for the detailed investigation!

 -- Stephen Kitt <skitt@debian.org>  Sun, 29 Jun 2014 19:14:34 +0200

mail-notification (5.4.dfsg.1-11) unstable; urgency=medium

  * Support building with Evolution 3.12.2 (closes: #750313).

 -- Stephen Kitt <skitt@debian.org>  Tue, 03 Jun 2014 22:42:10 +0200

mail-notification (5.4.dfsg.1-10) unstable; urgency=medium

  * Support building with the EDS 3.11 API (closes: #744117).

 -- Stephen Kitt <skitt@debian.org>  Fri, 11 Apr 2014 06:49:01 +0200

mail-notification (5.4.dfsg.1-9) unstable; urgency=medium

  * Specify the prefix when configuring (closes: #735829).
  * Adopt the package (closes: #733146).
  * Move the Homepage control field to the source section.
  * Update the watch file (thanks to Bart Martens).
  * Standards-Version 3.9.5, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 18 Jan 2014 11:43:36 +0100

mail-notification (5.4.dfsg.1-8) unstable; urgency=low

  * Add build-dependency on libecal1.2-dev, required for evolution-data-
    server 3.8 (thanks to Iain Lane, synced from Ubuntu).

 -- Stephen Kitt <skitt@debian.org>  Fri, 05 Jul 2013 19:03:22 +0200

mail-notification (5.4.dfsg.1-7) unstable; urgency=low

  * Drop version on libgnomevfs2-extra (it was missing an epoch and all
    versions in the archive are recent enough).
  * Merge Ubuntu patches (thanks to Mathieu Trudel-Lapierre, Jargon Scott,
    Gunnar Hjalmarsson and Martin Pitt for taking care of the package
    there!):
    - eds3.5.patch: port to the EDS 3.5 API;
    - window-sizing.patch: fix window sizing for the mailbox selection
      dialog so it's usable;
    - ssl-issue.patch: prevent crashes when using SSL.
  * Check for maintainer mode correctly; this fixes the -Werror issue
    (closes: #707429).
  * Depend on gnome-keyring, it's required for storing passwords.
  * Enable hardening.
  * Use autotools-dev to update config.guess and config.sub.
  * Standards-Version 3.9.4, no change required.
  * Switch to my Debian address.

 -- Stephen Kitt <skitt@debian.org>  Tue, 21 May 2013 10:29:17 +0200

mail-notification (5.4.dfsg.1-6) unstable; urgency=low

  * Allow linking with --as-needed (thanks to Michael Bienia for the
    patch).
  * Support building with Evolution 3.4 (thanks to Michael Biebl and
    Mathieu Trudel-Lapierre for the patch; closes: #677455).

 -- Stephen Kitt <steve@sk2.org>  Tue, 19 Jun 2012 00:03:39 +0200

mail-notification (5.4.dfsg.1-5) unstable; urgency=low

  * Explicitly build-depend on libdbus-glib-1-dev for dbus-binding-tool.
  * wrap-and-sort the control files.

 -- Stephen Kitt <steve@sk2.org>  Wed, 28 Mar 2012 07:22:53 +0200

mail-notification (5.4.dfsg.1-4) unstable; urgency=low

  * Build with gmime 2.6 rather than gmime 2.4 (patch provided by Michael
    Biebl; closes: #664000).
  * Refresh patch providing Evolution 3.0 (from upstream, based on work by
    Erik van Pienbroek for Fedora; closes: #657558).
  * Depend on gstreamer0.10-tools which is required to play sounds
    (thanks to Sergio Cipolla for pointing this out; closes: #658607).
  * Ignore default values which can no longer be found (patch provided by
    Gunnar Hjalmarsson; closes: #641937).
  * Standards-Version 3.9.3, no change required.

 -- Stephen Kitt <steve@sk2.org>  Tue, 27 Mar 2012 00:06:19 +0200

mail-notification (5.4.dfsg.1-3) unstable; urgency=low

  * Add myself to uploaders and acknowledge NMUs (closes: #569777).
  * Calculate evolution version for dependencies, to allow building with
    Evolution 3.0 and Evolution 3.2 (so a binNMU should do the trick when
    the latter is uploaded to unstable).
  * Drop unused dependency on libgnomeprintui2.2-dev (patch provided by
    Michael Biebl; closes: #644569).

 -- Stephen Kitt <steve@sk2.org>  Fri, 09 Dec 2011 18:22:28 +0100

mail-notification (5.4.dfsg.1-2.5) unstable; urgency=low

  * Non-maintainer upload.
  * Update for Evolution 3.0 and GTK+ 3 (closes: #639228, #639966).
    Patches adapted from work done for Fedora Core 16.
  * Add stricter dependencies on Evolution, as suggested by Josselin
    Mouette.
  * Enable SSL, interpreting Jean-Yves Lefort's statements in
    https://launchpad.net/ubuntu/+bug/44335/comments/86 as sufficient
    clarification (see http://bugs.debian.org/286672#68 for details;
    closes: #286672, #458363; LP: #44335).
  * Avoid segfaulting if no display is found for the systray icon (patch
    provided by Julien Danjou; closes: #500880).
  * Correct format string error in the Polish translation (patch provided
    by Przemysław <prometheus@o2.pl>; closes: #523873).
  * Don't use <span> elements in notifications (closes: #588998).
  * Standards-Version 3.9.2, no change required.
  * Add build-arch and build-indep targets.
  * Document lintian override.
  * Remove Encoding key from .desktop files.

 -- Stephen Kitt <steve@sk2.org>  Mon, 05 Sep 2011 00:21:06 +0200

mail-notification (5.4.dfsg.1-2.4) unstable; urgency=low

  * Non-maintainer upload, with maintainer approval.
  * Correctly support Evolution 2.32 (closes: #617771).
  * Ignore mail marked as junk (closes: #547287).
  * Thanks to Michel Dänzer for both these fixes!

 -- Stephen Kitt <steve@sk2.org>  Sun, 13 Mar 2011 23:17:35 +0100

mail-notification (5.4.dfsg.1-2.3) unstable; urgency=low

  * Non-maintainer upload.
  * Build with gmime 2.4 (closes: #549057). Thanks to Götz Watsch for
    the original patch, adapted here to modify the gob sources.
  * Build with evolution-data-server 2.32.
  * Link in libX11 explicitly to build with strict linking dependencies.
  * Document all patches (minimally).
  * Standards-Version 3.9.1, no change required.

 -- Stephen Kitt <steve@sk2.org>  Mon, 28 Feb 2011 00:14:07 +0100

mail-notification (5.4.dfsg.1-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Switch to "3.0 (quilt)" source format, to allow the various changes
    below to be readily identified.
  * Split existing changes into patches, and fix the existing change to
    mn-evolution-server.gob for evolution-data-server 2.23.5 (the
    eds-version.h include file was missing).
  * Update .gob files for gob2 2.0.16 and rebuild from scratch (Closes:
    #580886).
  * Update for evolution-data-server 2.29 and later, based on the fix
    present in Fedora 13
    (https://bugzilla.redhat.com/attachment.cgi?id=388098) (Closes:
    #580884, #582118).
  * Replace stock icons with icons present in current versions of
    gnome-icon-theme, based on the fix present in Fedora 13
    (https://bugzilla.redhat.com/attachment.cgi?id=415509) (Closes:
    #577503).

 -- Stephen Kitt <steve@sk2.org>  Wed, 26 May 2010 06:58:44 +0200

mail-notification (5.4.dfsg.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add include paths for libgtkhtml (Closes: #549745, #559106,
    #564036). Thanks Yavor Doganov!
  * Standards-Version 3.8.4, no change required.

 -- Stephen Kitt <steve@sk2.org>  Sat, 13 Feb 2010 14:13:38 +0100

mail-notification (5.4.dfsg.1-2) unstable; urgency=low

  * New maintainer (Closes: #520263)
  * Remove the redundant build dependence -- libeel2-dev (Closes: #525545)
  * Change the debian patch system to git vcs system
  * debian/rules: remove dh_desktop
  * debian/mail-notification.install: enable autostart (Closes: #531156)
  * Upgrade the standard version and make some clean on debian directory.
  * debian/copyright: update the Debian copyright file.
  * Rebuild will put the mail-notification-evolution plugins in the right
    directory. (Closes: #393606, #535904)
  * po/de.po: Fix typo in German locale. (Closes: #522886)
  * update the manpage. (Closes: #522556)
  * debian/control: make mail fetchers recommended instead of suggested
    (Closes: #512100)

 -- LIU Qi <liuqi82@gmail.com>  Sun, 19 Jul 2009 17:03:06 +0800

mail-notification (5.4.dfsg.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * [debian/patches/02-fix_evolution_crash_on_startup.diff]:
    - Apply patch from Ubuntu to fix Evolution 2.24 crashing on startup
      with this plugin enabled.

 -- Ari Pollak <ari@debian.org>  Tue, 07 Apr 2009 12:07:37 -0400

mail-notification (5.4.dfsg.1-1) unstable; urgency=medium

  * New upstream release
    (closes: #460307, #325931, #461459, #404183, #487492, #309033),
    (LP: #184125, #41048, #93703, #223803).
  * [debian/copyright]:
    - Code is now licensed under the GNU General Public License version 3.
    - Documentation is now licensed under the GNU Free Documentation License
      version 1.2 with no Invariant Sections, no Front-Cover Texts, and no
      Back-Cover Texts.
  * [debian/control]:
    - Removed Build-Depends on cdbs, automake and libtool, replaced by upstream.
    - Removed Build-Depends on gob2 for now.
    - Added getlive and fetchyahoo (Hotmail and Yahoo! support) to Suggests.
    - Moved homepage from package descriptions to its own field.
    - Updated Standards-Version to 3.8.0.1, no changes required.
  * [debian/patches/04-fixed_polish_sent.diff,
     debian/patches/06-fix_evolution_plugin_check.diff,
     debian/patches/07-fix_API_change.diff,
     debian/patches/08-freedesktop.org_specs_compliance.diff]:
    - Removed, fixed upstream.
  * [debian/patches/01-gmail.png_has_been_replaced-AUTHORS.diff]:
    - Updated to fit new upstream.
  * [debian/patches/02-per_user_autostart-01.diff,
     debian/patches/02-per_user_autostart-02.diff,
     debian/patches/05-add_french_autostart.diff]:
    - Removed for now, still-unreleased gob2 >= 2.0.16 is required.
  * [debian/rules]:
    - Now building using Jean-Yves Lefort replacement for GNU Autotools.
    - No longer using cdbs.
  * [debian/linda-override]: Removed, linda is no more.
  * [debian/README.Debian]:
    - Removed autostart from mail-notification is patched is disabled.
    - Added information about configuring default mail reader.
    - Added information about Yahoo! and Hotmail support.
  * [debian/watch]:
    - Updated to version 3.
    - Added dversionmangling option to remove dfsg string.

 -- Pascal Giard <pascal@debian.org>  Wed, 30 Jul 2008 13:52:24 -0400

mail-notification (4.1.dfsg.1-4.1) unstable; urgency=high

  * Non-maintainer upload to fix two RC bugs preventing multiple transitions
    to testing.
  * Call automake-1.10 explicitely as the automake alternative might point to
    any version and break the build; closes: #460300.
  * Drop incorrect chrpath -d call and chrpath build-dep; closes: #453788.
    This is fine as it's not a public library but a plugin.

 -- Loic Minier <lool@dooz.org>  Fri, 11 Jan 2008 22:58:43 +0100

mail-notification (4.1.dfsg.1-4) unstable; urgency=low

  * Mostly Debian package cleanups, fixed all lintian warnings.
  * [debian/control]:
    - Renamed ${Source-Version} to ${binary:Version}.
    - Added Build-Dep on chrpath.
    - Added Depends on ${misc:Depends} (gconf2).
  * [debian/rules]:
    - Fixed possible library search path interference by deleting rpath
      of liborg-jylefort-mail-notification.so.
    - Build in debian/mail-notification so dh_gconf finds schemas (LP: #134519).
  * [debian/mail-notification.menu]:
    - Renamed depricated section Apps to Applications and subsection
      Net to Network.
  * [debian/patches/08-freedesktop.org_specs_compliance.diff]:
    - Removed Application category, comply with FreeDesktop menu specifications.
    - Added Email category to both desktop entries.
  * [debian/watch]:
    - Fixed file pattern as upstream moved to bzip.

 -- Pascal Giard <pascal@debian.org>  Sat, 29 Sep 2007 02:14:20 -0400

mail-notification (4.1.dfsg.1-3) unstable; urgency=low

  * Support for Evolution >= 2.11 (closes: #444162).
  * Merge with current Ubuntu version, thanks to Cesare Tirabassi.
  * [debian/control]:
    - Add depend on libgnomevfs2-extra (>= 2.18).
    - Add Build-Depends on automake (>= 1.10), libtool (>= 1.5) and
      intltool (>= 0.35).
  * [debian/patches/06-fix_evolution_plugin_check.diff]:
    - Fix unversioned evolution plugin starting with evolution-dev 2.11.
      + Add evolution-plugin-nv.m4 macro.
      + Modify configure.ac .
  * [debian/patches/07-fix_API_change.diff]:
    - Fix API change for em_folder_tree_set_selected.
  * [debian/rules]:
    - Modify EVODIR definition and usage.
    - Delete EVOVERSION, no longer needed.
    - Add reconfiguration in post-patches target.
    - Fix debian-rules-ignores-make-clean-error lintian warning.
    - Delete files regenerated in post-patches target.

 -- Pascal Giard <pascal@debian.org>  Wed, 26 Sep 2007 15:14:39 -0400

mail-notification (4.1.dfsg.1-2) unstable; urgency=low

  * Rebuilt against libgmime2.0-2 2.2.10 (closes: #434329).

 -- Pascal Giard <pascal@debian.org>  Sat, 11 Aug 2007 09:09:50 -0400

mail-notification (4.1.dfsg.1-1) unstable; urgency=low

  * New upstream release (closes: #421330, #287040).
  * [debian/copyright]:
   - Updated documentation license as it's now under the GNU Free Documentation
     License. It's still okay for main since invariant/covers are excluded.
     Opinion based on http://www.debian.org/vote/2006/vote_001 .
  * [debian/patches/06-mail-notif-ssl.diff]: Removed, applied upstream.
  * [debian/patches/05-add_french_autostart.diff]: Updated to new release.
  * [debian/rules]: Updated evolution filenames, renamed upstream.

 -- Pascal Giard <pascal@debian.org>  Thu, 05 Jul 2007 02:20:47 -0400

mail-notification (4.0.dfsg.1-2) unstable; urgency=low

  * [debian/control]:
    - Added missing dependency on notification-daemon (closes: #427888).
  * [debian/patches/06-mail-notif-ssl.diff]:
    - Added patch preventing mail-notification from sending passwords in cleartext when SSL
      is unavailable (closes: #428157, #429200). Thanks to Ted Percival <ted@midg3t.net>.

 -- Pascal Giard <pascal@debian.org>  Tue, 26 Jun 2007 00:18:05 -0400

mail-notification (4.0.dfsg.1-1) unstable; urgency=low

  * New upstream release.
  * [debian/control]:
    - Added version to mail-notification-evolution depend.
    - Added version to libnotify-dev depend (closes: #409119).

 -- Pascal Giard <pascal@debian.org>  Sun, 15 Apr 2007 14:28:23 -0400

mail-notification (4.0~rc2.dfsg.1-5) experimental; urgency=low

  * Experimental upload for evolution 2.8.

 -- Pascal Giard <pascal@debian.org>  Thu, 14 Dec 2006 02:28:42 -0500

mail-notification (4.0~rc2.dfsg.1-4) unstable; urgency=low

  * [debian/rules]: Really fix FTBFS for feisty.

 -- Pascal Giard <pascal@debian.org>  Thu, 14 Dec 2006 01:34:45 -0500

mail-notification (4.0~rc2.dfsg.1-3) unstable; urgency=low

  * [debian/rules]: Fixes FTBFS for evolution >= 2.9 (e.g. feisty).

 -- Pascal Giard <pascal@debian.org>  Thu, 14 Dec 2006 01:34:15 -0500

mail-notification (4.0~rc2.dfsg.1-2) experimental; urgency=low

  * Experimental upload.
  * [debian/control]: Evolution 2.8 support.
  * [debian/rules]: Better detection of evolution's version, supporting epoch.

 -- Pascal Giard <pascal@debian.org>  Wed, 13 Dec 2006 10:18:57 -0500

mail-notification (4.0~rc2.dfsg.1-1) unstable; urgency=low

  * New upstream version (closes: #354121).
  * [debian/rules]:
    - Using the autogenerated .desktop files.
    - Removed a .desktop file copy that wasn't doing anything.
    - Fixed lintian warning by deleting old config.log.
  * [debian/control]:
    - Added support for mozilla mailboxes to description.
    - Added build-deps on libnotify-dev.
  * [debian/patches/03-proper_cmd_handling.diff]:
    - Deleted, changes to upstream renders this patch obsolete.
  * [debian/patches/06-evolution_browsing_fix.diff]:
    - Deleted, fixed in upstream.
  * [debian/patches/02-per_user_autostart.diff,
     debian/patches/05-add_french_autostart.diff]:
    - Updated to new upstream.
  * [debian/mail-notification.xpm]:
    - Updated to latest artwork.

 -- Pascal Giard <pascal@debian.org>  Mon, 11 Dec 2006 01:46:05 -0500

mail-notification (3.0.dfsg.1-10) unstable; urgency=low

  * [debian/rules]: Improved regexp detecting evolution-dev version.
  * [debian/patches/06-evolution_browsing_fix.diff]:
    Fixes issues with browsing evolution folders (closes: #388174).
  * [debian/control]: build-deps modifications had to be reversed for unstable.

 -- Pascal Giard <pascal@debian.org>  Fri, 24 Nov 2006 22:52:46 -0500

mail-notification (3.0.dfsg.1-9) experimental; urgency=low

  * Rebuild with experimental build-deps.
    - Bump up build-deps to evolution-dev, and evolution-plugins >= 2.8.0 and
      evolution-data-server-dev >= 1.8.0 to pull experimental versions.

 -- Loic Minier <lool@dooz.org>  Thu, 16 Nov 2006 10:39:20 +0100

mail-notification (3.0.dfsg.1-8) unstable; urgency=low

  * [debian/control]:
    Build-Depends, evolution-dev and evolution-plugins bumped to >= 2.6 .
  * [debian/rules]: Detecting evolution-dev version (closes: #393606).
  * [debian/patches/02-per_user_autostart.diff.old]:
    Deleted forgotten file.

 -- Pascal Giard <pascal@debian.org>  Sat, 04 Nov 2006 13:29:33 -0500

mail-notification (3.0.dfsg.1-7) unstable; urgency=low

  * [debian/patches/02-per_user_autostart.diff]:
    Fixed patch, don't try to create ~/.config/autostart if it exists.
  * [debian/patches/05-add_french_autostart.diff]:
    Fixed grammar error.

 -- Pascal Giard <pascal@debian.org>  Tue, 12 Sep 2006 23:15:42 -0400

mail-notification (3.0.dfsg.1-6) unstable; urgency=low

  * [debian/patches/04-fixed_polish_sent.diff]:
    Fixed mistake in polish translation of "Sent" word as "Posłanę".
    It's now "Wysłane" thanks to Radoslaw Warowny <radoslaww@gmail.com>
    (closes: #351988).
  * [debian/patches/05-add_french_autostart.diff]:
    French translation of "Autostart Mail Notification upon session opening".
    Goes along with debian/patches/02-per_user_autostart.diff.
  * [debian/patches/02-per_user_autostart.diff]:
    - Improved patch to update toggle button based on file existence. Another
      application may disable autostart or delete/create the autostart file.
    - Improved patch to create autostart directory if it doesn't exists.

 -- Pascal Giard <pascal@debian.org>  Sat, 04 Sep 2006 06:42:37 -0400

mail-notification (3.0.dfsg.1-5) unstable; urgency=low

  * [debian/control]:
    - Added mail-notification-evolution to Suggests.
    - Added gob2 to Build-Depends so that c files can be regenerated.
  * [debian/rules]:
    - Delete autogenerated /etc/xdg/autostart directory.
    - Delete generated src/mn-properties-dialog*.{h,c}.
  * [debian/postinst]: Delete old /etc/xdg/autostart files for transition.
  * [debian/patches/02-per_user_autostart.diff]:
    Per user autostart via configuration dialog (closes: #384133).
  * [debian/mail-notification.desktop]:
    Deleted, using the one generated by upstream.
  * [debian/patches/03-proper_cmd_handling.diff]:
    Proper command handling thanks to Michael Rasmussen <mir@datanom.net>
    (closes: #376274).
  * [debian/README.Debian]:
    - Add URL to SSL Debian BTS thread for more information.
    - Clarified/corrected menu path to mail-notification (closes: #384946).

 -- Pascal Giard <pascal@debian.org>  Wed, 23 Aug 2006 12:37:12 -0400

mail-notification (3.0.dfsg.1-4) unstable; urgency=low

  * [debian/control]:
    - Build-Depends on libgmime-2.0-2-dev (>=2.2.3) instead of 2.1
      (closes: #383159).
    - Removed duplicate Depends on evolution.

 -- Pascal Giard <pascal@debian.org>  Mon, 21 Aug 2006 08:31:21 -0400

mail-notification (3.0.dfsg.1-3) unstable; urgency=low

  * [debian/rules]: Fixed bashisms (closes: #377361).

 -- Pascal Giard <pascal@debian.org>  Sat, 08 Jul 2006 13:14:27 -0400

mail-notification (3.0.dfsg.1-2.1) unstable; urgency=low

  * [debian/changelog]:
    Really closing "dependency on evolution" bug (closes: #376217).

 -- Pascal Giard <pascal@debian.org>  Mon, 03 Jul 2006 19:35:34 -0400

mail-notification (3.0.dfsg.1-2) unstable; urgency=low

  * [debian/rules]: Using dh_desktop.
  * [debian/control]: Evolution support in a seperate package (closes: #376217).
  * [debian/rules]: Evolution support in a seperate package.
  * [debian/dirs]: Removed /etc/xdg/autostart, dh_desktop takes care of it.
  * [debian/mail-notification-preferences.desktop]:
    - Removed, rendered useless by autogenerated
      mail-notification-properties.desktop.

 -- Pascal Giard <pascal@debian.org>  Mon, 03 Jul 2006 10:57:14 -0400

mail-notification (3.0.dfsg.1-1) unstable; urgency=low

  * New upstream version (might close #351256).
  * Thanks to Oystein Gisnas <oystein@gisnas.net> for his help!
  * [debian/patches/01-gmail.png_has_been_replaced-AUTHORS.diff]:
    - Updated for new upstream.
  * [debian/rules,debian/control]:
    - Added Evolution support (closes: #322971).
    - Bumped Standards-Version to 3.7.2.0, added -fPIC to comply with
      Policy 10.2.
  * [debian/patches/00-Official-mail-notification-2.0-buildfix.diff]:
    - Removed, applied upstream.
  * [debian/control]: Removed libssl build-dep, ssl is still disabled.
  * [debian/postinst]: Removed, menus are handled by debhelper.
  * [debian/dirs]: Removed useless /usr/share/gnome .
  * [debian/mail-notification.desktop]: Fixed typo and added icon.
  * [debian/mail-notification-preferences.desktop]: Added settings menu entry.
  * [debian/mail-notification.1]: Added Evolution support.

 -- Pascal Giard <pascal@debian.org>  Mon, 26 Jun 2006 09:21:44 -0400

mail-notification (2.0.dfsg.1-3) unstable; urgency=low

  * Rebuilt package to depend on libfam0 instead of libgamin0
    (closes: #356958).
  * [debian/mail-notification.desktop]:
     - mail-notification is automatically started, thanks to
       Sam Morris <sam@robots.org.uk> (closes: #327773).
  * [debian/dirs]:
     - creating /etc/xdg/autostart for desktop file.
  * [debian/rules]:
     - added linker flags.

 -- Pascal Giard <pascal@debian.org>  Mon, 24 Apr 2006 13:48:20 -0400

mail-notification (2.0.dfsg.1-2) unstable; urgency=low

  * Rebuilt package to depend on libgnome-menu2 instead of
    libgnome-menu0 (closes: #348119).

 -- Pascal Giard <pascal@debian.org>  Sun, 15 Jan 2006 10:55:56 -0500

mail-notification (2.0.dfsg.1-1) unstable; urgency=low

  * [debian/control]:
     - Removed build dependency on libbonobo-activation-dev as
       bonobo-activation is depricated (closes: #332813).
  * Replaced the problematic gmail.png made by Google, see:
    - http://lists.debian.org/debian-legal/2005/08/msg00273.html
  * [debian/patches/02-gmail.png_has_been_replaced-AUTHORS.diff]:
    - Removed note about "included" Google art.
    - Added a note about the new logo.

 -- Pascal Giard <pascal@debian.org>  Tue, 08 Nov 2005 19:48:14 -0500

mail-notification (2.0-1) unstable; urgency=low

  * New upstream release (closes: #322630, #304490, #314825).
  * [debian/README.Debian, debian/control]:
    - Gmail support has been fixed by upstream.
  * [debian/patches/01-mail-notification-1.1.better_de.po.diff]:
    - Removed, merged in upstream.
  * [debian/patches/*]:
    - Added the upstream build fixes.
  * [debian/control]:
    - libsoup2.2-dev and libicu28-dev are no longer required.
    - Compliant to Policy Manual 3.6.2.1.

 -- Pascal Giard <pascal@debian.org>  Thu, 11 Aug 2005 15:50:04 -0400

mail-notification (1.1-4) unstable; urgency=low

  * [debian/patches/01-mail-notification-1.1.better_de.po.diff]:
    - Corrections in german translation (closes: bug#313798).
    Thanks to Jens Seidel <jensseidel@users.sf.net>.
  * [debian/README.Debian]: Updated with latest errata.
  * [debian/control]: Added a note about Gmail support being broken.

 -- Pascal Giard <pascal@debian.org>  Thu, 04 Aug 2005 11:36:53 -0400

mail-notification (1.1-3) unstable; urgency=low

  * [debian/patches/00-mail-notification-1.1.better_pl.po.diff]:
    - Improved translation, most parts should be there now. Thanks to Marcin
    <masamune@op.pl>.

 -- Pascal Giard <pascal@debian.org>  Tue, 15 Mar 2005 20:03:22 -0500

mail-notification (1.1-2) unstable; urgency=low

  * [debian/control]: New dependency on gnome-icon-theme (closes: bug#298531).
  * [debian/patches/00-mail-notification-1.1.better_pl.po.diff]:
    - Better but incomplete polish translation by Marcin <masamune@op.pl>.

 -- Pascal Giard <pascal@debian.org>  Tue, 08 Mar 2005 21:25:28 -0500

mail-notification (1.1-1) unstable; urgency=low

  * New upstream release.
  * [debian/rules]: Keeping mail-notification.png, needed (closes: bug#295063?).
  * [debian/control]: New build-depend on libicu28-dev.

 -- Pascal Giard <pascal@debian.org>  Mon, 07 Mar 2005 01:21:39 -0500

mail-notification (1.0-3) unstable; urgency=low

  * [debian/control]: Fixed short description.

 -- Pascal Giard <pascal@debian.org>  Sun, 30 Jan 2005 19:10:20 -0500

mail-notification (1.0-2) unstable; urgency=low

  * [debian/watch]: Added for uupdate, taken from Dan Korostelev.
  * [debian/config, debian/templates]:
    Dropping debconf template (closes: bug#290869).

 -- Pascal Giard <evilynux@yahoo.com>  Sun, 30 Jan 2005 16:52:20 -0500

mail-notification (1.0-1) unstable; urgency=low

  * New upstream release.
  * [debian/rules]:
    - Disabled SSL because of a license issue.
    - Removed gnome help path fix as it is fixed in upstream.
  * [debian/control]: Added build-depends: libsoup2.2-dev (>= 2.2.0) and
    libgmime2.1-dev.
  * [debian/mail-notification.1]: Integrated pieces from the manpage
    written by Dan Korostelev <dan@ats.energo.ru>.
  * [debian/linda-override]: No more linda warnings, thanks to Dan Korostelev.
  * [debian/copyright]: Added Gnome manual copyright.

 -- Pascal Giard <evilynux@yahoo.com>  Wed, 01 Dec 2004 20:19:13 -0500

mail-notification (0.6.2-1) unstable; urgency=low

  * Initial Release (closes: bug#271701).
  * Added manpage.
  * Fixed wrong gnome help path.
  * Converted the 48x48 png menu icon to a 32x32 xpm file with 24 colors.
  * Added menu entries for mail-notification and its preferences dialog
    in Apps/Net and Apps/System.

 -- Pascal Giard <evilynux@yahoo.com>  Thu, 30 Sep 2004 00:50:57 -0400

